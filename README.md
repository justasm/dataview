DataView
========
A set of views for displaying data on Android. In development.

Line Chart
----------
XML:

    <com.justasm.dataview.LineChart
    android:id="@+id/lineChart"
    android:layout_width="match_parent"
    android:layout_height="@dimen/chart_height" />

Java:

    LineChart chart = (LineChart) findViewById(R.id.lineChart);
    AutoSeries dataSeries = new AutoSeries(chart, "data", DataType.TIME, DataType.GENERIC);
    chart.addSeries(dataSeries);

    dataSeries.add(1, 3);
    dataSeries.add(2, 7);
    dataSeries.add(3, 4);