package com.justasm.dataview;

import java.lang.ref.WeakReference;

import android.graphics.Color;
import android.graphics.PointF;

public class AutoSeries extends SimpleSeries {
	
	private static final int DEFAULT_DATA_COLOR = Color.parseColor("#00CC00");
	private static final int DEFAULT_DATA_FILL_COLOR = Color.parseColor("#2200CC00");
	private int dataColor = DEFAULT_DATA_COLOR;
	private int dataFillColor = DEFAULT_DATA_FILL_COLOR;
	
	private final String name;
	private final WeakReference<LineChart> chartRef;
	
	private final DataType xDataType;
	private final DataType yDataType;
	
	public AutoSeries(LineChart chart, String name, DataType xDataType, DataType yDataType){
		this.name = name;
		this.xDataType = xDataType;
		this.yDataType = yDataType;
		chartRef = new WeakReference<LineChart>(chart);
	}
	
	@Override
	public PointF add(float x, float y){
		PointF newPoint = super.add(x, y);
		LineChart chart = chartRef.get();
		if(null != chart){
			chart.onAdd(newPoint);
		}
		return newPoint;
	}
	
	@Override
	public void clear(){
		super.clear();
		LineChart chart = chartRef.get();
		if(null != chart){
			chart.onClear();
		}
	}
	
	@Override
	public DataType getXType() {
		return xDataType;
	}
	
	@Override
	public DataType getYType() {
		return yDataType;
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	public void setColor(int color){
		dataColor = color;
	}
	
	@Override
	public int getColor() {
		return dataColor;
	}
	
	public void setFillColor(int color){
		dataFillColor = color;
	}
	
	@Override
	public int getFillColor() {
		return dataFillColor;
	}
	
}
