package com.justasm.dataview;

import java.util.ArrayList;
import java.util.List;

import android.graphics.PointF;

public abstract class SimpleSeries implements DataSeries {
	
	private final List<PointF> data;
	
	public SimpleSeries(){
		data = new ArrayList<PointF>();
	}
	
	public PointF add(float x, float y){
		PointF newPoint = new PointF(x, y);
		data.add(newPoint);
		return newPoint;
	}
	
	public void clear(){
		data.clear();
	}
	
	@Override
	public int getCount() {
		return data.size();
	}
	
	@Override
	public PointF get(int position) {
		return data.get(position);
	}
	
	@Override
	public PointF getLatest() {
		return data.get(data.size() - 1);
	}
	
}
